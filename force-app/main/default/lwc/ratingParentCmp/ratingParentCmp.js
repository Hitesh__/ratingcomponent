import {
    LightningElement,
    track
} from 'lwc';

export default class RatingParentCmp extends LightningElement {

    @track readOnly = false;
    @track rating = 0;

    //Method to handle rating change event fired by ratingCMP component
    handleRatingChanged(event) {
        this.rating = event.target.value;
    }
}