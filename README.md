# View uploaded files as thumbnails

![star_2.png](https://bitbucket.org/repo/R5qyzxK/images/4017498272-star_2.png)

## Functional Details

This is a rating component, which can be used in various usecases like providing a product review on a scale of 5( or more ).

## Technical Details

This component uses a third party library, which is need to be imported in the js file of our component. Compress [this](https://bitbucket.org/Hitesh__/ratingcomponent/src/master/starZip/) 
folder into a zip and upload it as a static resource with name **fivestar** (if you wanted another name for your static resource then 
update the name in **ratingCMP.js** while importing the static resource).

You just have to include **ratingCMP** component as a child component ( as shown in [ratingParentCmp](https://bitbucket.org/Hitesh__/ratingcomponent/src/master/force-app/main/default/lwc/ratingParentCmp/))

API Params -> 

1. readOnly - If you wanted to show this cmp in a read only mode then pass **true** in this param and false otherwise.
2. value - As clear by name, this param holds the rating value given by the user, passed from parent cmp to child cmp.

As of now the rating is set on a scale of 5, you can increase/decrease the rating limit by updating **maxRating** param in **ratingCMP.js**.
